package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x==null || y==null)throw new IllegalArgumentException();
        if(x.isEmpty() && !y.isEmpty()) return true;
        else if(x.isEmpty() && y.isEmpty()) return true;
        if(!x.isEmpty() && !y.isEmpty()){
            int n=0;
            for (int i = 0; i <y.size() ; i++) {
                if(x.get(n).equals(y.get(i))) ++n;
                if(n==x.size())return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Subsequence subsequence = new Subsequence();
        List x = null;
        List y = new ArrayList();

        //run
        System.out.println(subsequence.find(x, y));
    }
}
