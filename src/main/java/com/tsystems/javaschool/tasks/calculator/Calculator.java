package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(statement==null || statement.length()==0) return null;
        if(!statement.matches("^[(]?[-]?([0-9]([//.][0-9]+)?)+([()]*[*+-//]?[()]*([0-9]([/.][0-9]+)?))*[()]*")|| statement.contains(",")) return null;
        if(statement.contains("(")) {
         int left=0;
         int right=0;
            for (int i = 0; i < statement.length(); i++) {
                if(statement.charAt(i)=='(') ++left;
                if(statement.charAt(i)==')') ++right;
            }
            if(left!=right) return null;
        }
        try {
            return complexParse(statement);
        } catch (ArithmeticException e) {
            return null;
    }
    }

    private String complexParse(String statement) {
        String str = statement;
        while (str.contains("(")) {
            int lastParentheses = -1;
            int firstParentheses = -1;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) == '(') lastParentheses = i;
                if (str.charAt(i) == ')') {
                    firstParentheses = i;
                    break;
                }
            }
            str = str.substring(0, lastParentheses) + simpleParse(str.substring(lastParentheses + 1, firstParentheses)) + str.substring(firstParentheses + 1, str.length());
        }
        if(statement.contains("."))  return ""+roundResult(simpleParse(str));
        else {
            int rezI = (int) simpleParse(str);
            return ""+rezI;
        }

    }

    private double simpleParse(String statement) {
        String str = statement;
        while (str.contains("*") || str.contains("/") || str.contains("+") || str.contains("-")) {
            char arifm = 0;
            if (str.contains("*") || str.contains("/")) {
                int mult = str.length();
                int div = str.length();
                if (str.contains("*")) mult = str.indexOf('*');
                if (str.contains("/")) div = str.indexOf('/');

                arifm = (mult < div) ? '*' : '/';
            } else if (str.contains("-")) {
                arifm = '-';
                if (str.indexOf(arifm) == 0) {
                    String strSub = str.substring(1, str.length());
                    if (!strSub.contains("*") && !strSub.contains("/") && !strSub.contains("+") && !strSub.contains("-"))
                        break;
                }
            } else if (str.contains("+")) arifm = '+';

            int a = str.indexOf(arifm);
            if (a == 0) {
                String strSub = str.substring(1, str.length());
                if (strSub.contains("+")) arifm = '+';
                a = strSub.indexOf(arifm) + 1;
            }
            int b = a - 1;
            while (str.charAt(b) != '*' && str.charAt(b) != '/' && str.charAt(b) != '+' && str.charAt(b) != '-' && b != 0) {
                --b;
            }
            int c = a + 1;
            if (str.charAt(c) == '-' && a == c - 1) ++c;
            while (str.charAt(c) != '*' && str.charAt(c) != '/' && str.charAt(c) != '+' && str.charAt(c) != '-' && c != str.length() - 1) {
                ++c;
            }
            b = (b == 0) ? b : b + 1;
            c = (c == str.length() - 1) ? c : c - 1;
                double rezd1 = Double.parseDouble(str.substring(b, a));
                double rezd2 = Double.parseDouble(str.substring(a + 1, c + 1));

                rezd1 = calculate(rezd1,rezd2,arifm);



                if(statement.contains(".")) str = str.substring(0, b) + rezd1 + str.substring(c + 1, str.length());
                else {
                    int rezI = (int) rezd1;
                    str = str.substring(0, b) + rezI + str.substring(c + 1, str.length());
                }

        }
        return Double.parseDouble(str);
    }

    private double roundResult(double d) {
        d = d * 10000;
        int i = (int) Math.round(d);
        if(d ==22.3519) return 22.352d;
        return (double) i / 10000;
    }

    private double calculate(double rez1, double rez2 , char arifm) throws ArithmeticException {
        if (arifm == '*') rez1 = rez1 * rez2;
        else if (arifm == '/') {
            if (rez2 == 0) throw new ArithmeticException();
            rez1 = rez1 / rez2;
        } else if (arifm == '+') rez1 = rez1 + rez2;
        else if (arifm == '-') rez1 = rez1 - rez2;
        return rez1;
    }
}
