package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        int height = (int) Math.sqrt(1+4*2*inputNumbers.size());// найдем высоту массива
        double heightFor = Math.sqrt(1+4*2*inputNumbers.size());
        if(heightFor>height)throw new CannotBuildPyramidException();
        if(height==0)  throw new CannotBuildPyramidException();
        height = (height-1)/2;
        int weight = height+height-1; // ширина массива
        int[][] ar = new int [height][weight];
        Collections.sort(inputNumbers);
        int start = 1+weight/2;
        int pointer = 0;
        for (int i = 0; i <ar.length ; i++) {
            int now = start-1;
            for (int j = 0; j <=i ; j++) {
                ar[i][now]=inputNumbers.get(pointer);
                now+=2;
                ++pointer;
            }
            --start;
        }
        return ar;
    }
}
